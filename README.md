## LEDC
## The purpose of this campaign is to help bring awareness to young professionals and technology companies about why London is a destination city. 
- Digital presence focusing on:
    - Web, Social, Marketing.
Create a large scale advertising campaign around attracting tech talent to be used across multiple platforms. 

## Target Audiences:
- Students and young professionals within and outside of London looking for careers in the tech sector. 
- Skilled, older professionals within and outside of London looking for careers in the tech sector. 
- Technology companies within and outside of London that are looking to recruit. 

## Getting started:
Project checklist is active on the Trello-board if you are needing ideas or a guideline towards getting started. 

## Prerequisites 
- Atom 
- MAMP
- Dreamweaver 
- Cinema 4D

## Installing
- Atom: https://atom.io/
- MAMP: https://www.mamp.info/en/
- Dreamweaver (With Creative Cloud): http://www.adobe-students.com/creativecloud/buy/students.html?sdid=KKTHT&mv=search&s_kwcid=AL!3085!3!221150330581!e!!!!dreamweaver&ef_id=VTmOoQAAAdvZhzMW:20170928001954:s
- Dreamweaver separate: https://www.adobe.com/ca/products/dreamweaver.html
- Cinema 4D: https://www.maxon.net/en/try/demo-download/

## Running the tests
- W3C Markup Validation 
    - Built into Atom 
        - Atom automatically checks for W3C markup validation. 
    - External: https://validator.w3.org/#validate_by_input
        - All codes must go through the validator 
    - CSS: http://www.css-validator.org/
        - All codes must go through the validator 
    - Preview chrome / console
## Deployment
- Still yet to figure out hosting options. 
- Development team: what works best for you?

## Built with
- Atom - Generated HTML5, CSS, Javascript
- MAMP - SQL & Databasing
- Dreamweaver - Generated HTML5, CSS, Javascript
- Cinema 4D - Graphics and designs 
- SASS: CSS Designer & Codewriter 
 - http://sass-lang.com/
- Grunt: Javascript Task Runner
 - https://github.com/gruntjs/grunt
- Node: Javascript Runtime built on Chrome's V8 Javascript Engine
 - https://nodejs.org/en/

## Contributing
- Back end Developer - Shane Fry
- Front end Developer - Jeongyun Cho
- Good contributing guidelines: https://gist.github.com/PurpleBooth/b24679402957c63ec426

## Versioning

## Authors
- Shane Fry - Back end Development
- Jeongyun Cho - Front end Development 
- Lexi Sterio - Development Review
- Jorden McColl - Development Review
- Blake Ferguson - Development Review 

## Licence: 
Project is based on behalf of LEDC. Final products will be published on their use or behalf.